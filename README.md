# Download from any instagram profile:

## How to use:

Clone the repo via git:

```sh
git clone https://bitbucket.org/Arijoon/ig-downloader.git ig-downloader
```

install npm packages

```sh
cd ig-downloader
npm install
```

Create a folder named `result` in this directory

run the script with arguments:

```sh
node index.js [username|profile link] [arguments]
```

example, download from profile name "sport" the last 20 images and skip the first 3

```sh
node index.js sport -n 20 -s 3
```

All arguments:

| Switch | Description                     |
|--------|---------------------------------|
| n      | number of images from last post |
| s      | how many to skip                |

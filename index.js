const rp = require('request-promise'),
    http = require('https'),
    fs = require('fs'),
    path = require('path'),
    opt = require('optimist');

const _userId = /{"id":.?"(\d+)"}/,
    _queryId = "17880160963012870",
    _api = "https://www.instagram.com/graphql/query/", // GET with query_id, id, first
    _base = "https://www.instagram.com";

const commonRegex = {
    link: /(https?:\/\/\S+)/,
    usermention: /<@\d+>/
};

const resultPath = './result';

(function main() {

    let argv = setupOptions();
    let ops = argv.argv

    // Exit the application
    if (ops.h) {
        console.log(argv.help())
        process.exit(0);
    }

    let url = commonRegex.link.test(ops._[0])
        ? commonRegex.link.exec(ops._[0])[0]
        : `${_base}/${ops._[0]}`

    get(url)
        .then(res => {

            let match = _userId.exec(res);
            id = match[1]

            return getJson(`${_api}?query_id=${_queryId}&id=${id}&first=${ops.s || ops.n}`);
        }).then(res => {
            if (ops.s) {
                let end = res.data.user.edge_owner_to_timeline_media.page_info.end_cursor;
                return getJson(`${_api}?query_id=${_queryId}&id=${id}&first=${ops.n}&after=${end}`);
            }

            return res;
        }).then(res => {
            let nodes = res.data.user.edge_owner_to_timeline_media.edges;
            let results = []

            for (let node of nodes) {
                results.unshift(node.node.display_url);
            }

            return results;
        }).then(res => {
            log(`Saving result, count: ${res.length}`)

            var promise = (function recursiveDownload(res, batchSize) {
                const promises = [];
                let counter = 0;

                while(counter++ < batchSize) {
                    if(res.length <= 0) {
                        return Promise.all(promises);
                    }
                    let url = res.shift();
                    let itemSplit = url.split('/');
                    let filename = itemSplit[itemSplit.length-1];
                    promises.push(downloadFile(url, path.join(resultPath, filename)));
                }

                return Promise.all(promises).then(() => recursiveDownload(res, batchSize))
            })(res, 20);
            
            return promise;
        }).then(() => {
            console.log(`Saved all files`)
        }).catch(err => {
            console.error(err);
        });
}) ();



function downloadFile(url, dest) {
    return new Promise((resolve, reject) => {

        var file = fs.createWriteStream(dest);
        var request = http.get(url, function (response) {
            response.pipe(file);
            file.on('finish', function () {
                file.close();  // close() is async, call cb after close completes.
                log(`Finished downloading: ${dest}`);
                resolve(dest);
            });
        }).on('error', function (err) { // Handle errors
            fs.unlink(dest); // Delete the file async. (But we don't check the result)
            console.error(`failed to download: ${dest}, error: ${err.message}`);
            reject(err);
        });
    });
}

function log(msg) {
    console.log(msg);
}

function get(url) {
    let options = {
        url: url,
        json: false
    }

    return rp(options);
}

function getJson(url) {
    let options = {
        uri: url,
        json: true
    }

    return rp(options);
}

function setupOptions() {
    var argv = opt
        .options('n', {
            alias: 'number',
            describe: 'specify the number of images to get',
            default: 1
        }).options('s', {
            alias: 'skip',
            describe: 'specify the number of images to skip',
            default: null
        }).options('h', {
            alias: 'help',
            describe: 'show this message',
            default: false
        });

    return argv;
}